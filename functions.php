<?php

function delete_task(int $task_id)
{
    global $conn;
    $sql='';
    $sql = 'delete from tasks where id='.$task_id;
    $result = $conn->query($sql);
}

function add_task(string $title , string $owner , string $status)
{
    global $conn;
    $sql = "INSERT INTO  tasks (title , owner , status) VALUES(\"$title\" , \"$owner\" , \"$status\")";
    $insertResult = $conn->query($sql);
    if($insertResult)
    {
        $last_id = $conn->insert_id;
        return $last_id;
    }
    return -1;
}

function edit_task(string $title , string $owner , string $status, int $id)
{
    global $conn;
    $sql = "UPDATE  tasks set title=\"$title\" , owner=\"$owner\" , status=\"$status\" where id=$id ";
    $editResult = $conn->query($sql);
    if($editResult)
    {
        return true;
    }
    return false;
}

function get_tasks(string $where)
{
    global $conn;
    $sql = 'SELECT * FROM '.Task_Table.$where;
    $result = $conn->query($sql);
    $tasks = $result->fetch_all(MYSQLI_ASSOC);
    return $tasks;
}

function call_current_page()
{
    $uri_parts = explode('?', $_SERVER['REQUEST_URI'], 2);
    $currentPage = 'http://' . $_SERVER['HTTP_HOST'] . $uri_parts[0];
    header("location: $currentPage");
}

function get_owners($table_name, $column_name)
{
   global $conn;
   $sql = 'SHOW COLUMNS FROM '.$table_name.' WHERE field="'.$column_name.'"';
   $row = $conn->query($sql)->fetch_all();
   $enumList = explode(",", str_replace("'", "", substr($row[0][1], 5, (strlen($row[0][1])-6))));
   $i=0;
   $owner_array = array();
   foreach($enumList as $item)
   {
        $i+=1;
        $owner_array["$i"] = $item;
   }
   return $owner_array;
}

<?php include_once("config_restful.php"); ?>

<?php 
    $where = '';
    $restful_url = '';
    switch (true) {
        case (isset($_GET['del'])):
            $restful_url = Site_Address.'restfulpages/delete_task.php'.'?del='.$_GET['del'];
            file_get_contents($restful_url);
            $restful_url = Site_Address.'restfulpages/select_all.php';
            break;
        case (isset($_GET['status'])):
            $restful_url = Site_Address.'restfulpages/select_status_filter.php'.'?status='.$_GET['status'];
            break;
        default:
            $restful_url = Site_Address.'restfulpages/select_all.php';
            break;
    }
    $restful_json = file_get_contents($restful_url);
    $tasks = (object)json_decode($restful_json , true);

    //get owners
    $restful_url = Site_Address.'restfulpages/get_owners.php';
    $owners_json = file_get_contents($restful_url);    
    $owners = json_decode($owners_json , true);
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Task manager UI</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style.css">
</head>

<body>
    <!-- partial:index.partial.html -->
    <div class="page">
        <div class="pageHeader">
            <div class="title">Dashboard</div>
            <div class="userPanel"><i class="fa fa-chevron-down"></i><span class="username">John Doe </span><img src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/73.jpg" width="40" height="40" /></div>
        </div>
        <div class="main">
            <div class="nav">
                <div class="searchbox">
                    <div><i class="fa fa-search"></i>
                        <input type="search" placeholder="Search" />
                    </div>
                </div>
                <div class="menu">
                    <div class="title">Navigation</div>
                    <ul>
                        <li> <i class="fa fa-home"></i>Home</li>
                        <li><i class="fa fa-signal"></i>Activity</li>
                        <li class="active"> <i class="fa fa-tasks"></i>Manage Tasks</li>
                        <li> <i class="fa fa-envelope"></i>Messages</li>
                    </ul>
                </div>
            </div>
            <div class="view">
                <div class="viewHeader">
                    <div class="title">Manage Tasks</div>
                    <div class="functions">
                        <div class="button active" id="addNewTask">Add Task</div>
                        <div class="button"><a href="?status=1" class="filter_href">Draft</a></div>
                        <div class="button"><a href="?status=2" class="filter_href">Todo</a></div>
                        <div class="button"><a href="?status=3" class="filter_href">Done</a></div>
                        <div class="button"><a href="?status=4" class="filter_href">Archive</a></div>
                        <div class="button"><a href="?" class="filter_href">All</a></div>
                        <!--<div class="button inverz"><i class="fa fa-trash-o"></i></div>-->
                    </div>
                </div>
                <div class="content">
                    <div class="list">
                        <div class="title staticdiv">Today</div>
                        <ul>
                            <?php foreach($tasks as $task) : $task = (object)$task; ?>
                            <li class="checked" id="taskrecord_<?= $task->id ?>">
                                <?php if($task->status >= 3)
                                {
                                    echo('<i class="fa fa-check-square-o"></i>');
                                } 
                                else
                                {
                                    echo('<i class="fa fa-square"></i>');
                                } ?>
                                <span class="taskTitle"><?= $task->title ?></span>
                                <div class="info">
                                    <span class="taskOwner" id="taskOwner<?= $task->id ?>"><?= $task->owner ?></span>
                                    <i name = "editTaskIcon" data-taskrecord="taskrecord_<?= $task->id ?>"  data-taskid="<?= $task->id ?>" class="fa fa-pencil-square-o editTaskIcon" aria-hidden="true" style="color:darkslateblue"></i>
                                    <a href="?del=<?= $task->id ?>" style="color:red"><i class="fa fa-trash-o"></i></a>
                                    <div class="button green taskStatus" data-status="<?= $task->status?>" id="taskStatus<?= $task->id ?>"><?= $task_statuses[$task->status] ?></div>
                                    <span style="font-size: 0.75rem;">Created at : <?= $task->created_at ?></span>
                                </div>
                            </li>
                            <?php endforeach; ?> 
                            <div id="ajaxResult"></div>
                        </ul>
                    </div>
                    <div class="list">
                        <div class="title">Tomorrow</div>
                        <ul>
                            <li><i class="fa fa-square-o"></i><span>Find front end developer</span>
                                <div class="info"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php include_once ("pages/pageModal.php"); ?>
        </div>
    </div>
    <!-- partial -->
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="assets/script.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        $(document).ready(function(){
            $var_Add_or_Update = '';
            $taskRecord = '';
            //$("#addNewTask").click(function(){
            $(document).on('click', '#addNewTask', function(){
                $var_Add_or_Update = 'Add';
                $("#submitModal").val("Add");
                $("#modalLabel").text("Add new task");
                $("#modalTaskTitle").val('');
                $("#modalTaskOwner").val($("#modalTaskOwner option:first").val());
                $("#modalTaskStatus").val($("#modalTaskStatus option:first").val());
                $("#modalActionType").val("Add");
                $("#modalTaskId").val("0");
                $('#taskModal').modal('show');
            }); //end $("#addNewTask").click

            $(document).on('click', '.editTaskIcon', function(){
                $var_Add_or_Update = 'Edit';
                $("#submitModal").val("Edit");
                $("#modalLabel").text("Edit task");

                $("#modalTaskTitle").val('');
                $("#modalTaskOwner").val('');
                $("#modalTaskStatus").val('');
                $("#modalActionType").val("");
                $("#modalTaskId").val(-1);

                taskRecord = $(this).data("taskrecord");
                taskid = $(this).data("taskid");
                var taskTitle_CurrentRecord = $('#'+ taskRecord + ' .taskTitle').text();
                var taskOwner_CurrentRecord = $('#'+ taskRecord + ' .taskOwner').text();
                var taskStatus_CurrentRecord = $('#'+ taskRecord + ' .taskStatus').data('status');
                $("#modalTaskTitle").val(taskTitle_CurrentRecord);
                $("#modalTaskowner").val(taskOwner_CurrentRecord);
                $("#modalTaskStatus").val(taskStatus_CurrentRecord);

                $("#modalActionType").val("Edit");
                $("#modalTaskId").val(taskid);
                $('#taskModal').modal('show');
            });

            $("#submitModal").click(function(e){
                e.preventDefault();
                var url='';
                if($var_Add_or_Update=='Add')
                {
                    url = '<?=Site_Address?>'+'restfulpages/add_task.php';
                }
                else
                {
                    url = '<?=Site_Address?>'+'restfulpages/edit_task.php';
                }
                $.ajax({
                    type:"POST",
                    data : $("#modalForm").serialize(),
                    url : url,
                    success:function(response)
                    {
                        var response_array = '';
                        var html_response_array = '';
                        response_array = $.parseJSON(response);
                        html_response_array = createNewContent(response_array['title'] , 
                                                               response_array['owner'] , 
                                                               response_array['status'] , 
                                                               response_array['id'], 
                                                               response_array['datetime']);    
                        if($var_Add_or_Update=='Add')
                        {
                            $("#ajaxResult").append(html_response_array);
                        }
                        else
                        {
                            $('#'+ taskRecord).replaceWith(html_response_array);
                        }
                        $("#taskModal").modal("hide");

                    },
                    error:function(response){
                        $("#ajaxResult").html("There is a problem in server side.");
                        $("#taskModal").modal("hide");
                    }
                });
            });

            function createNewContent(taskTitle , taskOwner , taskStatus , taskId, taskDatetime)
            {
                var task_statuses_var = [];
                task_statuses_var["1"]='Draft';
                task_statuses_var["2"]='Todo';
                task_statuses_var["3"]='Done';
                task_statuses_var["4"]='Archive';
                var htmlResult = '';
                htmlResult += '<li class="checked" id="taskrecord_'+taskId+'">';
                if(taskStatus >= 3)
                {
                    htmlResult += '<i class="fa fa-check-square-o"></i>';
                } 
                else
                {
                    htmlResult += '<i class="fa fa-square"></i>';
                } 
                htmlResult += '<span class="taskTitle">'+taskTitle+'</span>';
                htmlResult += '<div class="info">';
                htmlResult += '<span class="taskOwner">'+taskOwner+'</span>';
                htmlResult += '<i name = "editTaskIcon" data-taskrecord="taskrecord_'+taskId+'"  data-taskid="'+taskId+'" class="fa fa-pencil-square-o editTaskIcon" aria-hidden="true" style="color:darkslateblue"></i>';
                htmlResult += '<a href="?del='+taskId+'" style="color:red"><i class="fa fa-trash-o"></i></a>';
                htmlResult += '<div class="button green taskStatus" data-status="'+taskStatus+'" >'+task_statuses_var[taskStatus]+'</div>';
                htmlResult += '<span style="font-size: 0.75rem;">Created at : '+taskDatetime+'</span>';
                htmlResult += '</div>';
                return htmlResult;
            }

        });//end $(document).ready
    </script>
</body>

</html>
<?php 
include_once("../functions.php");  
include_once("../config_restful.php"); 

date_default_timezone_set('Europe/Istanbul'); 
$datetime = date("Y-m-d H:i:s"); 
$id = 0;
$resultHTML='';
if($_POST["modalActionType"]=="Edit")
{
    $editResult = edit_task($_POST["modalTaskTitle"] , $_POST["modalTaskowner"] , $_POST["modalTaskStatus"],$_POST["modalTaskId"]);
    $id=$_POST["modalTaskId"];
    if ($editResult==true)
    {
        $edited_record = array();
        $edited_record["id"] = $editResult;
        $edited_record["title"] = $_POST["modalTaskTitle"];
        $edited_record["owner"] = $_POST["modalTaskowner"];
        $edited_record["status"] = $_POST["modalTaskStatus"];
        $edited_record["datetime"] = $datetime;
    }
    else
    {
        $edited_record = array();
        $edited_record["id"] = $editResult;
    }
}
$edited_record_json = json_encode($edited_record);
echo ($edited_record_json);

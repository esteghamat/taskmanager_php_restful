<?php 
include_once("../functions.php");  
include_once("../config_restful.php"); 

date_default_timezone_set('Europe/Istanbul'); 
$datetime = date("Y-m-d H:i:s"); 
$id = 0;
$resultHTML='';
if($_POST["modalActionType"]=="Add")
{
    $insertResult = add_task($_POST["modalTaskTitle"] , $_POST["modalTaskowner"] , $_POST["modalTaskStatus"]);
    $id=$insertResult;
    if ($insertResult>0)
    {
        $new_record = array();
        $new_record["id"] = $insertResult;
        $new_record["title"] = $_POST["modalTaskTitle"];
        $new_record["owner"] = $_POST["modalTaskowner"];
        $new_record["status"] = $_POST["modalTaskStatus"];
        $new_record["datetime"] = $datetime;

    }
    else
    {
        $new_record = array();
        $new_record["id"] = $insertResult;
    }
}
$new_record_json = json_encode($new_record);
echo ($new_record_json);


//$resultHTML=createNewContent($_POST["modalTaskTitle"] , $_POST["modalTaskowner"] , $_POST["modalTaskStatus"],$id,$datetime);


